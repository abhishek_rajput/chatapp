package com.example.hp.chatapp.presenter;

import android.net.Uri;

public interface ChatPresenter {

    void sendMessage(String message, String dateTime, String sender, String receiver);

    void sendPhoto(String dateTime, String sender, String receiver, String photoUrl);

    void receiveMessage(String myId , String userId);

    void uploadImageToFirebaseStorage(Uri uriImage, String myId , String userId);
}
