package com.example.hp.chatapp.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.widget.Toast;

public class Utils<Data> {

    public static void getIntent(Context context, Class destination, int flag) {
        Intent intent = new Intent(context, destination);
        intent.setFlags(flag);
        context.startActivity(intent);

    }

    public static void showMessage(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public Intent setIntentExtra(Context context, Class destination, String key, Data data) {
        Intent intent = new Intent(context, destination);
        intent.putExtra(key, (Parcelable) data);
        context.startActivity(intent);

        return intent;
    }

}

