package com.example.hp.chatapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.ProgressBar;

import com.example.hp.chatapp.R;
import com.example.hp.chatapp.presenter.SignUpPresenterImpl;
import com.example.hp.chatapp.presenter.SignUpView;
import com.example.hp.chatapp.utils.Utils;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, SignUpView {

    private TextInputEditText etUserEmail, etUserPassword, etUserConPassword, etUserName;
    private ProgressBar progressBar;
    private SignUpPresenterImpl signUpPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initViews();
        signUpPresenter = new SignUpPresenterImpl(this);


    }

    private void initViews() {
        etUserEmail = findViewById(R.id.et_Email);
        etUserName = findViewById(R.id.et_name);
        etUserPassword = findViewById(R.id.et_Pass);
        progressBar = findViewById(R.id.progress);
        etUserConPassword = findViewById(R.id.et_con_Password);
        findViewById(R.id.buttonSignUp).setOnClickListener(this);
        findViewById(R.id.log_in).setOnClickListener(this);
    }

    private void validationCheck() {

        String email = etUserEmail.getText().toString().trim();
        String password = etUserPassword.getText().toString().trim();
        String conPassword = etUserConPassword.getText().toString().trim();
        String name = etUserName.getText().toString().trim();

        if (name.isEmpty()) {
            etUserName.setError("Name required");
            etUserName.requestFocus();
            return;
        }

        if (email.isEmpty()) {
            etUserEmail.setError("Email required");
            etUserEmail.requestFocus();
            return;

        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etUserEmail.setError("Please Enter The Valid Email");
            etUserEmail.requestFocus();
            return;
        }
        if (password.isEmpty()) {
            etUserPassword.setError("Password required");
            etUserPassword.requestFocus();
            return;
        }
        if (password.length() < 6) {
            etUserPassword.setError("Password Is Short Please Enter Minimum 6 Digit");
            etUserPassword.requestFocus();
            return;
        }

        if (conPassword.isEmpty()) {
            etUserConPassword.setError("Confirm Password required");
            etUserConPassword.requestFocus();
            return;
        }
        if (!conPassword.equals(password)) {
            etUserConPassword.setError("Password Not Matched");
            etUserConPassword.requestFocus();
            return;
        }
        signUpPresenter.signUp(email, password, name);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonSignUp:
                validationCheck();
                break;
            case R.id.log_in:
                Utils.getIntent(this, LoginActivity.class,Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();

        }
    }

    @Override
    public void signUpSuccess() {
        Utils.showMessage(this, "Successfully Register");
        Utils.getIntent(this, AllUsersActivity.class,Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
    }

    @Override
    public void onError(String error) {
        Utils.showMessage(this, error);

    }

    @Override
    public void setProgressVisibility(boolean visibility) {
        if (visibility)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);

    }
}