package com.example.hp.chatapp.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.example.hp.chatapp.R;
import com.example.hp.chatapp.modal.User;
import com.example.hp.chatapp.utils.OnIemClickListener;

import java.util.ArrayList;


public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.PostViewHolder> {

    private ArrayList<User> usersList;
    private OnIemClickListener onIemClickListener;

    public UserListAdapter(ArrayList<User> users, OnIemClickListener onIemClickListener) {
        this.usersList = users;
        this.onIemClickListener = onIemClickListener;
    }

    @NonNull
    @Override
    public UserListAdapter.PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.user_item_view, parent, false);
        return new PostViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PostViewHolder holder, final int position) {

        final User list = usersList.get(holder.getAdapterPosition());
        holder.userName.setText(list.getName());



        String letters = String.valueOf(list.getName().charAt(0));
        ColorGenerator colorGenerator = ColorGenerator.MATERIAL;
        int color = colorGenerator.getColor(list);

        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .withBorder(30)
                .endConfig()
                .buildRoundRect(letters, color, 60);
        holder.uaerImage.setImageDrawable(drawable);

    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }


    class PostViewHolder extends RecyclerView.ViewHolder {
        ImageView uaerImage;
        TextView userName;
        CardView cardView;

        PostViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_view);
            uaerImage = itemView.findViewById(R.id.userImage);
            userName = itemView.findViewById(R.id.userName);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onIemClickListener.onClick(usersList.get(getAdapterPosition()));
                }
            });


        }

    }
}