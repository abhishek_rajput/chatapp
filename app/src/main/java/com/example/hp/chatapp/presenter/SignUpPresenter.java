package com.example.hp.chatapp.presenter;

public interface SignUpPresenter {

    void signUp(String email, String password, String name);
}
