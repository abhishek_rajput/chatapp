package com.example.hp.chatapp.utils;

public interface Constant {
    String USER_NAME = "user_name";
    String TYPE_MESSAGE = "message";
    String TYPE_PHOTO = "photo";
    int TEXT_RIGHT = 0;
    int TEXT_LEFT = 1;
    int PHOTO_RIGHT = 2;
    int PHOTO_LEFT = 3;
    int CHOOSE_PHOTO = 101;
}
