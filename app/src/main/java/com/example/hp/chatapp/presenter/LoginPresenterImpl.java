package com.example.hp.chatapp.presenter;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginPresenterImpl implements LoginPresenter {

    private LoginView loginView;


    public LoginPresenterImpl(LoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void userLogin(String email, String password) {
        loginView.setProgressVisibility(true);
        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                loginView.setProgressVisibility(false);
                if (task.isSuccessful()) {
                    loginView.loginSuccess();
                } else {
                    loginView.loginError();
                }
            }
        });

    }

    @Override
    public void checkLogin() {
        loginView.isLogin(FirebaseAuth.getInstance().getCurrentUser() != null);
    }
}


