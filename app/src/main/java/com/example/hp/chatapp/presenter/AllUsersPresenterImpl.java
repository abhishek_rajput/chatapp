package com.example.hp.chatapp.presenter;

import android.support.annotation.NonNull;

import com.example.hp.chatapp.modal.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class AllUsersPresenterImpl implements AllUsersPresenter {


    private AllUsersView allUsersView;
    private ArrayList<User> userList;
    private User user;

    public AllUsersPresenterImpl(AllUsersView allUsersView) {
        this.allUsersView = allUsersView;
    }


    @Override
    public void getUsers() {
        DatabaseReference databaseUsers = FirebaseDatabase.getInstance().getReference("Users");
        userList = new ArrayList<>();
        allUsersView.setProgressVisibility(true);

        databaseUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userList.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    user = postSnapshot.getValue(User.class);
                    if (!user.getEmail().equals(FirebaseAuth.getInstance().getCurrentUser()
                            .getEmail())) {
                        allUsersView.setProgressVisibility(false);
                        userList.add(user);
                    }
                }

                allUsersView.allUserList(userList);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
