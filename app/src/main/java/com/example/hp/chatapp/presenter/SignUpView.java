package com.example.hp.chatapp.presenter;

public interface SignUpView {

    void signUpSuccess();

    void onError(String error);

    void setProgressVisibility(boolean visibility);
}
