package com.example.hp.chatapp.presenter;

import com.example.hp.chatapp.modal.User;

import java.util.ArrayList;

public interface AllUsersView {

    void allUserList(ArrayList<User> users);

    void setProgressVisibility(boolean visibility);

}
