package com.example.hp.chatapp.presenter;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.example.hp.chatapp.modal.Chat;
import com.example.hp.chatapp.utils.Constant;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class ChatPresenterImpl implements ChatPresenter {

    private ArrayList<Chat> chats = new ArrayList<>();
    private ChatView chatView;


    public ChatPresenterImpl(ChatView chatView) {
        this.chatView = chatView;
    }


    @Override
    public void sendMessage(String message, String dateTime, String sender, String receiver) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("message", message);
        hashMap.put("sender", sender);
        hashMap.put("receiver", receiver);
        hashMap.put("datetime", dateTime);
        hashMap.put("type", Constant.TYPE_MESSAGE);
        reference.child("chats").push().setValue(hashMap);
    }

    @Override
    public void sendPhoto(String dateTime, String sender, String receiver, String photoUrl) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("photoUrl", photoUrl);
        hashMap.put("sender", sender);
        hashMap.put("receiver", receiver);
        hashMap.put("datetime", dateTime);
        hashMap.put("type", Constant.TYPE_PHOTO);
        reference.child("chats").push().setValue(hashMap);
    }

    @Override
    public void receiveMessage(final String myId, final String userId) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                chats.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Chat chat = snapshot.getValue(Chat.class);
                    if (chat.getReceiver().equals(myId) && chat.getSender().equals(userId) ||
                            chat.getReceiver().equals(userId) && chat.getSender().equals(myId)) {

                        chats.add(chat);
                    }
                    chatView.userChat(chats);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    @Override
    public void uploadImageToFirebaseStorage(Uri uriImage, final String myid, final String userId) {
        chatView.imageUploadResponse(false);
        final StorageReference profileReference =
                FirebaseStorage.getInstance()
                        .getReference("sendingPhotos/"
                                + FirebaseAuth.getInstance().getCurrentUser().getUid()
                                + ".jpg");
        if (uriImage != null) {
            chatView.setProgressVisibility(true);
            profileReference.putFile(uriImage)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            chatView.setProgressVisibility(false);
                            chatView.imageUploadResponse(true);
                            profileReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    Calendar cal = Calendar.getInstance();
                                    Date currentLocalTime = cal.getTime();
                                    SimpleDateFormat df = new SimpleDateFormat("dd-MMM hh:mm a", Locale.getDefault());
                                    String currentTime = df.format(currentLocalTime);
                                    sendPhoto(currentTime, myid, userId, uri.toString());

                                }
                            });

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    chatView.setProgressVisibility(false);
                    chatView.imageUploadResponse(true);
                    chatView.showError(e.getLocalizedMessage());
                }
            });
        }
    }
}



