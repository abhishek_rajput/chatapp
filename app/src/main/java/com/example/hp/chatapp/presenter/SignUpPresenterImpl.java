package com.example.hp.chatapp.presenter;

import android.support.annotation.NonNull;

import com.example.hp.chatapp.modal.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.FirebaseDatabase;

public class SignUpPresenterImpl implements SignUpPresenter {

    private SignUpView signUpView;
//    private FirebaseAuth auth;

    public SignUpPresenterImpl(SignUpView signUpView) {
        this.signUpView = signUpView;
//        this.auth = auth;
    }

    @Override
    public void signUp(final String email, String password, final String name) {

        signUpView.setProgressVisibility(true);
        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    User user = new User(
                            name,
                            email);
                    FirebaseDatabase.getInstance().getReference("Users")
                            .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                            .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            signUpView.setProgressVisibility(false);
                            if (task.isSuccessful()) {
                                signUpView.signUpSuccess();
                            } else {
                                signUpView.onError(task.getException().getLocalizedMessage());
                            }
                        }
                    });
                } else {
                    signUpView.setProgressVisibility(false);
                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                        signUpView.onError("Email Already Registered");
                    } else {
                        signUpView.onError(task.getException().getLocalizedMessage());
                    }
                }

            }


        });


    }
}


