package com.example.hp.chatapp.adapter;

import android.support.annotation.NonNull;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.hp.chatapp.R;
import com.example.hp.chatapp.modal.Chat;
import com.example.hp.chatapp.utils.Constant;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;


public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.PostViewHolder> {

    private ArrayList<Chat> chatList;
    private View view;


    public ChatAdapter(ArrayList<Chat> userChat) {
        this.chatList = userChat;
    }

    @NonNull
    @Override
    public ChatAdapter.PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == Constant.TEXT_RIGHT) {
            view = inflater.inflate(R.layout.text_chat_right_message, parent, false);
            return new PostViewHolder(view);
        } else if (viewType == Constant.TEXT_LEFT) {
            view = inflater.inflate(R.layout.text_chat_left_message, parent, false);
            return new PostViewHolder(view);
        } else if (viewType == Constant.PHOTO_RIGHT) {
            view = inflater.inflate(R.layout.photo_chat_right_message, parent, false);
            return new PostViewHolder(view);
        } else {
            view = inflater.inflate(R.layout.photo_chat_left_message, parent, false);
            return new PostViewHolder(view);
        }
    }


    @Override
    public void onBindViewHolder(@NonNull final PostViewHolder holder, final int position) {

        Chat chat = chatList.get(position);
        if (chat.getType().equals(Constant.TYPE_MESSAGE)) {
            holder.message.setText(chat.getMessage());
            holder.dateTime.setText(chat.getDateTime());
        } else {
            holder.photo.setVisibility(View.VISIBLE);

            if (holder.photo != null) {
                Glide.with(view.getContext())
                        .load(chat.getPhotoUrl())
                        .into(holder.photo);
                holder.dateTime.setText(chat.getDateTime());

            }
        }
    }


    @Override
    public int getItemCount() {
        return chatList.size();
    }

    @Override
    public int getItemViewType(int position) {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        Chat chat = chatList.get(position);
        String type = chat.getType();

        if (type.equals(Constant.TYPE_MESSAGE)) {
            if (chatList.get(position).getSender().equals(firebaseUser.getEmail())) {
                return Constant.TEXT_RIGHT;
            } else {
                return Constant.TEXT_LEFT;
            }
        } else {
            if (chatList.get(position).getSender().equals(firebaseUser.getEmail())) {
                return Constant.PHOTO_RIGHT;
            } else {
                return Constant.PHOTO_LEFT;
            }
        }
    }


    class PostViewHolder extends RecyclerView.ViewHolder {

        TextView message, dateTime;
        ImageView photo;

        PostViewHolder(View itemView) {
            super(itemView);
            dateTime = itemView.findViewById(R.id.message_time);
            message = itemView.findViewById(R.id.show_message);
            photo = itemView.findViewById(R.id.show_photo_message);

        }
    }
}