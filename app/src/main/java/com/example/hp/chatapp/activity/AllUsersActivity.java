package com.example.hp.chatapp.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.example.hp.chatapp.R;
import com.example.hp.chatapp.adapter.UserListAdapter;
import com.example.hp.chatapp.modal.User;
import com.example.hp.chatapp.presenter.AllUsersPresenter;
import com.example.hp.chatapp.presenter.AllUsersPresenterImpl;
import com.example.hp.chatapp.presenter.AllUsersView;
import com.example.hp.chatapp.utils.Constant;
import com.example.hp.chatapp.utils.OnIemClickListener;
import com.example.hp.chatapp.utils.Utils;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

public class AllUsersActivity extends AppCompatActivity implements OnIemClickListener, AllUsersView {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_users);
        progressBar = findViewById(R.id.progress);
        recyclerView = findViewById(R.id.recycler_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Users");
        setSupportActionBar(toolbar);
        AllUsersPresenter allUsersPresenter = new AllUsersPresenterImpl(this);
        allUsersPresenter.getUsers();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_log_out, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        logOut();
        return super.onOptionsItemSelected(item);
    }

    private void logOut() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Confirm Logout...");
        alertDialog.setMessage("Are you sure you want to Logout?");
        alertDialog.setIcon(R.drawable.ic_exit);
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                FirebaseAuth.getInstance().signOut();
                Utils.getIntent(AllUsersActivity.this, LoginActivity.class,Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                Utils.showMessage(AllUsersActivity.this, "Logout Successfully");

            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();

    }

    @Override
    public void onClick(User user) {
        Intent intent = new Intent(AllUsersActivity.this, ChatsActivity.class);
        intent.putExtra(Constant.USER_NAME, user);
        startActivity(intent);

    }

    @Override
    public void allUserList(ArrayList<User> users) {
        UserListAdapter userListAdapter = new UserListAdapter(users,
                AllUsersActivity.this);
        recyclerView.setAdapter(userListAdapter);
    }

    @Override
    public void setProgressVisibility(boolean visibility) {
        if (visibility)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }
}
