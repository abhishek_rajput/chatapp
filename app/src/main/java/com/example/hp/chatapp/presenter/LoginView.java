package com.example.hp.chatapp.presenter;


public interface LoginView {

    void loginSuccess();

    void loginError();

    void setProgressVisibility(boolean visibility);

    void isLogin(boolean isLogin);
}

