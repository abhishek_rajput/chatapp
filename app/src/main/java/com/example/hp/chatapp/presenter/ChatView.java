package com.example.hp.chatapp.presenter;

import com.example.hp.chatapp.modal.Chat;

import java.util.ArrayList;

public interface ChatView {

    void setProgressVisibility(boolean visibility);

    void showError(String error);

    void userChat(ArrayList<Chat> chats);

    void imageUploadResponse(boolean isUpload);
}
