package com.example.hp.chatapp.presenter;

import android.app.Activity;

public interface LoginPresenter {

    void userLogin(String email, String password);
    void checkLogin();
}
