package com.example.hp.chatapp.utils;

import com.example.hp.chatapp.modal.User;

public interface OnIemClickListener {

    void onClick(User data);

}
