package com.example.hp.chatapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.ProgressBar;

import com.example.hp.chatapp.R;
import com.example.hp.chatapp.presenter.LoginPresenter;
import com.example.hp.chatapp.presenter.LoginPresenterImpl;
import com.example.hp.chatapp.presenter.LoginView;
import com.example.hp.chatapp.utils.Utils;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, LoginView {


    private ProgressBar progressBar;
    private TextInputEditText etUserEmail, etUserPassword;
    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
        loginPresenter = new LoginPresenterImpl(this);
        loginPresenter.checkLogin();

    }

    private void initViews() {
        progressBar = findViewById(R.id.progress);
        etUserEmail = findViewById(R.id.user_email);
        etUserPassword = findViewById(R.id.user_password);
        findViewById(R.id.sign_up).setOnClickListener(this);
        findViewById(R.id.log_in).setOnClickListener(this);
    }

    public void validateLogin() {
        String email = etUserEmail.getText().toString().trim();
        String password = etUserPassword.getText().toString().trim();
        if (email.isEmpty()) {
            etUserEmail.setError("Email required");
            etUserEmail.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etUserEmail.setError("Please Enter The Valid Email");
            etUserEmail.requestFocus();
            return;
        }
        if (password.isEmpty()) {
            etUserPassword.setError("Password required");
            etUserPassword.requestFocus();
            return;
        }
        if (password.length() < 6) {
            etUserPassword.setError("Password Is Short Please Enter Minimum 6 Digit");
            etUserPassword.requestFocus();
            return;
        }

        loginPresenter.userLogin(email, password);

    }

    @Override
    public void loginSuccess() {
        Utils.showMessage(this, "Login Success !");
        Utils.getIntent(this, AllUsersActivity.class, Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
    }

    @Override
    public void loginError() {
        Utils.showMessage(this, "Login Error ! ");
    }


    @Override
    public void isLogin(boolean isLogin) {
        if (isLogin) {
            Utils.getIntent(this, AllUsersActivity.class,Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_up:
                Utils.getIntent(this, SignUpActivity.class,Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
            case R.id.log_in:
                validateLogin();
                break;
        }
    }

    @Override
    public void setProgressVisibility(boolean visibility) {
        if (visibility)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }
}