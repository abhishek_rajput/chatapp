package com.example.hp.chatapp.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.example.hp.chatapp.R;
import com.example.hp.chatapp.adapter.ChatAdapter;
import com.example.hp.chatapp.modal.Chat;
import com.example.hp.chatapp.modal.User;
import com.example.hp.chatapp.presenter.ChatPresenter;
import com.example.hp.chatapp.presenter.ChatPresenterImpl;
import com.example.hp.chatapp.presenter.ChatView;
import com.example.hp.chatapp.utils.Constant;
import com.example.hp.chatapp.utils.Utils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ChatsActivity extends AppCompatActivity implements ChatView {

    private EditText etTypeMessage;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private User user;
    private ImageButton sendMessage;
    private FirebaseUser currentUser;
    private Toolbar toolbar;
    private ChatPresenter chatPresenter;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats);
        initViews();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        chatPresenter = new ChatPresenterImpl(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getIntent().hasExtra(Constant.USER_NAME)) {
            user = getIntent().getParcelableExtra(Constant.USER_NAME);
        }
        getSupportActionBar().setTitle(user.getName());


        etTypeMessage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etTypeMessage.getRight() -
                            etTypeMessage.getCompoundDrawables()[DRAWABLE_RIGHT]
                                    .getBounds().width())) {
                        choosePhoto();
                        return true;
                    }
                }
                return false;
            }
        });

        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mess = etTypeMessage.getText().toString();
                if (!(mess.isEmpty())) {
                    Calendar cal = Calendar.getInstance();
                    Date currentLocalTime = cal.getTime();
                    SimpleDateFormat df = new SimpleDateFormat("dd-MMM hh:mm a", Locale.getDefault());
                    String currentTime = df.format(currentLocalTime);
                    chatPresenter.sendMessage(mess, currentTime, currentUser.getEmail(), user.getEmail());
                } else {
                    Utils.showMessage(ChatsActivity.this, "Type Message");
                }
                etTypeMessage.setText("");
            }
        });

        chatPresenter.receiveMessage(currentUser.getEmail(), user.getEmail());
    }

    private void initViews() {
        toolbar = findViewById(R.id.toolbar);
        etTypeMessage = findViewById(R.id.type_message);
        recyclerView = findViewById(R.id.chat_recycler_view);
        progressBar = findViewById(R.id.progress);
        sendMessage = findViewById(R.id.btn_send);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.CHOOSE_PHOTO && resultCode == RESULT_OK &&
                data != null && data.getData() != null) {

            Uri uriImage = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),
                        uriImage);
                chatPresenter.uploadImageToFirebaseStorage(uriImage, currentUser.getEmail(), user.getEmail());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constant.CHOOSE_PHOTO:
                if (grantResults.length > 0 && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED) {
                    choosePhoto();
                } else {
                    Utils.showMessage(this,
                            "Permission rejected you cant get photo from gallery");
                }
                break;
        }
    }

    private void choosePhoto() {
        try {
            if (ActivityCompat.checkSelfPermission
                    (ChatsActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(ChatsActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        Constant.CHOOSE_PHOTO);
            } else {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Choose Profile Photo"),
                        Constant.CHOOSE_PHOTO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void setProgressVisibility(boolean visibility) {
        if (visibility)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String error) {
        Utils.showMessage(this, error);
    }

    @Override
    public void userChat(ArrayList<Chat> chats) {
        ChatAdapter chatAdapter = new ChatAdapter(chats);
        recyclerView.setAdapter(chatAdapter);

    }

    @Override
    public void imageUploadResponse(boolean isUpload) {
        if (isUpload) {
            etTypeMessage.setEnabled(true);
        } else {
            etTypeMessage.setEnabled(false);
        }

    }
}
